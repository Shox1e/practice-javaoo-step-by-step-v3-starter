package ooss;

import java.util.Objects;

public class Student extends Person {
    private Klass klass;

    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        if (Objects.isNull(this.klass.getNumber())){
            return String.format("My name is %s. I am %d years old. I am a student.", this.name, this.age);
        }else if (!Objects.isNull(this.klass.getLeader())){
            return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.", this.name, this.age,this.klass.getNumber());
        }else {
            return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.", this.name, this.age,this.klass.getNumber());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(id, student.id);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public Boolean isIn(Klass klass) {
        if (Objects.isNull(this.klass))return false;
        else if (this.klass.equals(klass))return true;
        else return false;
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }
}
