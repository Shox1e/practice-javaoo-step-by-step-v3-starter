package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private List<Integer> klasslist;

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
        klasslist = new ArrayList<>();
    }

    @Override
    public String introduce() {
        StringBuilder res= new StringBuilder();
        res.append("My name is ").append(this.name).append(".").append(" I am ").append(this.age).append(" years old.").append(" I am a teacher.");
        if (!this.klasslist.isEmpty()){
            res.append(" I teach Class");
            for (Integer number : klasslist){
                res.append(" ").append(number).append(",");
            }
            res.deleteCharAt(res.length()-1).append(".");
        }
        return String.valueOf(res);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return Objects.equals(id, teacher.id);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void assignTo(Klass klass) {
        this.klasslist.add(klass.getNumber());
    }

    public boolean belongsTo(Klass klass) {
        if (this.klasslist.contains(klass.getNumber())) return true;
        else return false;
    }

    public boolean isTeaching(Student tom) {
        if (this.klasslist.contains(tom.getKlass().getNumber()))return true;
        return false;
    }
}
