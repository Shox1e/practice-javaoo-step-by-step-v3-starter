package ooss;

import java.util.Objects;

public class Klass {
    private Integer number;

    private Student leader;

    private Person observer;

    public Klass(Integer number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return Objects.equals(number, klass.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Student getLeader() {
        return leader;
    }

    public void setLeader(Student leader) {
        this.leader = leader;
    }

    public void assignLeader(Student tom) {
        if (!tom.isIn(this)){
            System.out.println("It is not one of us.");
        }else{
            this.leader = tom;
            if (this.observer instanceof Teacher){
                System.out.print(String.format("I am %s, teacher of Class %s. I know %s become Leader.",this.observer.name,this.number,this.leader.name));
            }else if (this.observer instanceof Student){
                System.out.print(String.format("I am %s, student of Class %s. I know %s become Leader.",this.observer.name,this.number,this.leader.name));
            }
        }
    }

    public boolean isLeader(Student tom) {
        if (tom.equals(this.leader))return true;
        return false;
    }

    public void attach(Person person) {
        this.observer = person;
    }
}
